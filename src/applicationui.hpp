/*
 * Copyright (c) 2011-2014 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ApplicationUI_HPP_
#define ApplicationUI_HPP_

#define JSON_FILE_PATH "data/jsonFile.json"

#include <QObject>
#include <bb/cascades/GroupDataModel>
#include <bb/data/JsonDataAccess>
#include <QFileSystemWatcher>

namespace bb
{
    namespace cascades
    {
        class LocaleHandler;
    }
}

class QTranslator;

/*!
 * @brief Application UI object
 *
 * Use this object to create and init app UI, to create context objects, to register the new meta types etc.
 */
class ApplicationUI : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bb::cascades::GroupDataModel* dataModel READ getDataModel WRITE setDataModel NOTIFY dataModelChanged)
public:
    ApplicationUI();
    virtual ~ApplicationUI();

    Q_INVOKABLE void addToJson(const QString& phoneNumber, const QString& message, const QDateTime& time);
    Q_INVOKABLE void clearJsonFile();

private slots:
    void onSystemLanguageChanged();
    void updateDataModel();

private:
    bb::cascades::GroupDataModel* getDataModel() { return m_dataModel; }
    void setDataModel(bb::cascades::GroupDataModel* newDataModel);

    QTranslator* m_pTranslator;
    bb::cascades::LocaleHandler* m_pLocaleHandler;

    bb::cascades::GroupDataModel* m_dataModel;
    bb::data::JsonDataAccess* jda;
    QFileSystemWatcher* watcher;

signals:
    void dataModelChanged();
};

#endif /* ApplicationUI_HPP_ */
